import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../services/api.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-films',
  templateUrl: './films.page.html',
  styleUrls: ['./films.page.scss'],
})
export class FilmsPage implements OnInit {

  films: Observable<any>;

   // constructor(private navController: NavController, private router: Router, private http: HttpClient) { }
  constructor(private navController: NavController, private router: Router, private api: ApiService) { }

  openDetails(filme) {
    // Both of these would work!
    // But the standard Router is recommended.
    // this.navController.navigateForward(`/tabs/films/42`);
    this.router.navigateByUrl(`/tabs/films/` + filme.episode_id);
    }

  goToPlanets() {
    this.navController.navigateRoot(`/tabs/planets`);
  }

  ngOnInit() {
    this.films = this.api.getFilms();
    /*
    this.films = this.http.get('https://swapi.co/api/films');
    this.films.subscribe(data => {
      console.log('my data: ', data);
    });
    */
  }
}